通用网关接入组件 openapi-client-provider 
===

# 简介
本组件是openapi-client组件的子组件，从openapi-client组件分离。根据openapi-client-core的框架结构，封装业务开发中需要的各种网关接入客户端，采用统一的模式和结构进行开发和沉淀。

地址：http://acooly.cn/docs/component.html?type=gateway

# 开发规范

## 组件规范

* 提供方名称规范：每个组件都必须有全局唯一的提供方名称。如：bosc,如果是同一个服务提供机构提供的多个服务（协议不同），则通过一下规范给提供方命名：providerName-xxx,如：bosc,bosc-b2b,bosc-account等。

* 模块命名规范：openapi-client-provider-${provider}-${providerProduct},其中providerProduct为可选，一般来说，如果该机构只有这个产品就可以省略（或者，你是第一个开发改提供方的人）

* 程序包命名：com.acooly.openapi.client.provider.${provider}.${providerProduct} ，同样的，你的组件模块的包名称必须全局唯一。因为，可能会在同一个目标工程中集成多个provider，同时在单元测试模块也会集成所有组件进行测试，所以如果你希望你的组件是通用且独立的，请一定要在单元测试中加入依赖并测试。

* 单元测试规范：每个组件必须在openapi-client-test模块中加入依赖，并测试你开发的组件。主要测试的目的包括：
    * 测试集成后，你的组件是否与其他组件冲突。
    * 测试你开发的客户端是否正常可用，包括配置文件。
    
* 模块文档：每个模块就是一个独立的网关客户端，必然有对应的网关提供的接口文档和说明文件，请以模块名称作为目录名称，提交相关的所有文档到该工程的wiki仓库。

* 模块说明文件：每个模块都必须编写说明文档（模块根目录下建立README.md文件）,需要在该文件中说明：改模块provider的简介（做什么的？解决什么问题？提供什么能力？），网关简介（协议简介，xml，json，签名模式，加密模式，测试地址，生产地址等），主要使用项目，负责人等

* 模块负责人：每个模块必须有专门的负责人。请在每个模块的根目录的README文件中标明。

* 模块独立性：所有provider模块放置在一个工程中的目的是为团队提供沉淀，但每个模块的完全独立的，每个模块的开发负责人（commiter）不能因为任何原因提交他人的模块代码。

 > 注意：以上规范的完整案例，请参考模块：openapi-client-provider-bosc
    
## 代码管理规范

* 该组件是基于苦力框架封装的各大银行和第三方支付的SDK沉淀，苦力框架提供的了丰富的基础组件库、应用组件库、门户组件库、业务组件库、OpenApi网关框架欢迎咨询

#### 基础组件库  http://acooly.cn/docs/component.html?type=infrastructure
* 短信发送组件

* web组件

* 防御组件

* 分布式缓存组件

* 对象存储访问组件

* JPA组件

* tomcat组件

* 过滤器组件

* 数据库连接池组件

* 文件上层组件

* 事件组件

* mybatis组件

* 线程池组件

* dubbo组件

* 序列号生成器组件

#### 应用组件库  http://acooly.cn/docs/component.html?type=app
* appservice组件

* 安全后台管理组件

* 移动端app组件

* 审计日志组件

* 实名/卡认证组件

* 图表组件

* 动态表单组件

* 邮件发送组件

* 分布式消息组件

* 验证码组件

* 微信接入组件

* im聊天组件

* pdf组件

* 分布式锁组件

* 白盒测试组件

* 分布式定时任务组件

* 加解密组件

* 全国区域组件

* 参数配置组件

#### 门户组件库 http://acooly.cn/docs/component.html?type=portlets
* 消息推送组件

* 行为日志组件

* 评论组件

* 反馈组件

#### 业务组件库  http://acooly.cn/docs/component.html?type=business

* 会员组件

* 抽奖组件

* 账务组件

* 积分组件

* 红包组件

* 游戏计数组件


#### OpenApi网关框架 http://acooly.cn/docs/component.html?type=openapi

* OpenApiSDK组件

* OpenApi接入开发指南

* OpenApi文档自动化，开放平台

* OpenApi服务异步通知

* OpenApi服务开发指南


## 捐赠
* 如果有小伙伴觉得对你有帮助或者需要咨询的欢迎添加我微信
![avatar](http://www.weichk.com/web/images/mywx.jpg)


# 如果你喜欢我们的开源请点个赞是对我们的奖励
# 如果你喜欢我们的开源请点个赞是对我们的奖励
# 如果你喜欢我们的开源请点个赞是对我们的奖励
