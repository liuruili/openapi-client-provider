package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytUnBindCardResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/19 22:06
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.UN_BIND_CARD,type = ApiMessageType.Response)
@XStreamAlias("message")
public class JytUnBindCardResponse extends JytResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private JytUnBindCardResponseBody unBindCardResponseBody;
}
