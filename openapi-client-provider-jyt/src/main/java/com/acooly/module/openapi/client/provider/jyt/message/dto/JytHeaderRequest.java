package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/** @author zhike 2018/5/7 23:34 */
@Getter
@Setter
@XStreamAlias("head")
public class JytHeaderRequest extends JytHeader {

}
