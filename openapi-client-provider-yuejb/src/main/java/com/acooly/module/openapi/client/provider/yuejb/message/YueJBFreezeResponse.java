package com.acooly.module.openapi.client.provider.yuejb.message;

import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBResponse;
import lombok.Data;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
@Data
public class YueJBFreezeResponse extends YueJBResponse {
    /**
     * 额度系统流水id
     */
    private String quotaBizId;
}
