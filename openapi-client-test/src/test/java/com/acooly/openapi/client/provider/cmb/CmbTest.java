package com.acooly.openapi.client.provider.cmb;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.cmb.CmbApiService;
import com.acooly.module.openapi.client.provider.cmb.message.HthDirectPayRequest;
import com.acooly.module.openapi.client.provider.cmb.message.HthDirectPayResponse;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@BootApp(sysName = "YlTest")
public class CmbTest extends NoWebTestBase {

    @Autowired
    private CmbApiService cmbApiService;

    String oid = Ids.gid();

    /**
     * 招商银企直连
     */
    @Test
    public void testRealDeduct() {

        HthDirectPayRequest request = new HthDirectPayRequest();

        //数据准备
        request.setMerchOrderNo(Ids.oid());
        request.setUserId("16103115491603800000");
        request.setAmount("300");
        request.setRecBankName("中信银行");
        request.setRecBankAddress("渝北支行");
        request.setRecBankCity("重庆");
        request.setRecBankProvince("重庆");
        request.setBankCardType("DEBIT_CARD");
        request.setTransUsage("下发");
        request.setInterbank("YES");//是否行内转帐

        request.setRecRealName("刘五");
        request.setRecBankCardNo("6225880230001175");
        request.setRecBankCode("CMB");
        request.setRecCertNo("500221198810192313");
        request.setRecMobileNo("18696725229");

        request.setPayAccountNo("591902896810104");
        request.setPayAccountName("银企直连专用账户10");
        request.setPayBankName("招商银行");
        request.setPayBankProvince("福州");
        request.setPayBankCity("福州");
        request.setPayBankAddress("福州分行");
        request.setPayBankCode("CMB");

        try {
            HthDirectPayResponse response = cmbApiService.hthDirectPayService(request);
            System.out.println("银联单笔代扣测试执行结果：" + JSON.toJSONString(response));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常原因：" + e.getMessage());
        }
    }

}


