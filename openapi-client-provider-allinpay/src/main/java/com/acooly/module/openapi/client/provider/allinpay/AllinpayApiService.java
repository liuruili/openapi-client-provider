/**
 * create by zhike
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.allinpay;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayNotify;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayRequestInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayResponsetInfo;
import com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayServiceEnum;
import com.acooly.module.openapi.client.provider.allinpay.message.*;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayBillDownloadRequestBody;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayBillDownloadResponseBody;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpaySinglePaymentRequestBody;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayTradeQueryRequestBody;
import com.acooly.module.openapi.client.provider.allinpay.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike
 */
@Service
public class AllinpayApiService {

    @Resource(name = "allinpayApiServiceClient")
    private AllinpayApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientAllinpayProperties properties;


    /**
     * 单笔实收代付
     * @param requestBody
     * @return
     */
    public AllinpaySinglePaymentResponse singlePayment(AllinpaySinglePaymentRequestBody requestBody) {
        AllinpaySinglePaymentRequest request = new AllinpaySinglePaymentRequest();
        requestBody.setMerchantId(properties.getPartnerId());
        request.setRequestInfo(getRequestInfo(AllinpayServiceEnum.SINGLE_PAYMENT.getKey(),"04",requestBody.getBizOrderNo()));
        request.setRequestBody(requestBody);
        return (AllinpaySinglePaymentResponse)apiServiceClient.execute(request);
    }

    /**
     * 交易查询
     * @param requestBody
     * @return
     */
    public AllinpayTradeQueryResponse tradeQuery(AllinpayTradeQueryRequestBody requestBody) {
        AllinpayTradeQueryRequest request = new AllinpayTradeQueryRequest();
        requestBody.setMerchantId(properties.getPartnerId());
        request.setRequestInfo(getRequestInfo(AllinpayServiceEnum.TRADE_QUERY.getKey(),"04",null));
        request.setRequestBody(requestBody);
        return (AllinpayTradeQueryResponse)apiServiceClient.execute(request);
    }

    /**
     * 对账文件下载
     * @param requestBody
     * @return
     */
    public AllinpayBillDownloadResponse billDownload(AllinpayBillDownloadRequestBody requestBody) {
        String filePath = properties.getFilePath();
        if(Strings.isBlank(filePath)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"文件路径不能为空");
        }
        AllinpayBillDownloadRequest request = new AllinpayBillDownloadRequest();
        requestBody.setMerchantId(properties.getPartnerId());
        request.setRequestInfo(getRequestInfo(AllinpayServiceEnum.BILL_DOWNLOAD.getKey(),"04",null));
        request.setRequestBody(requestBody);
        AllinpayBillDownloadResponse response = (AllinpayBillDownloadResponse)apiServiceClient.execute(request);
        AllinpayResponsetInfo responsetInfo = response.getResponsetInfo();
        if(Strings.equals(responsetInfo.getRetCode(),"0000")) {
            AllinpayBillDownloadResponseBody responseBody = response.getResponseBody();
            if(responseBody != null && Strings.equals(responseBody.getRetCode(),"0000")) {
                FileUtils.witeBill(response.getResponseBody().getContent(), filePath, requestBody.getStartDay());
            }
        }
        return response;
    }


    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public AllinpayNotify notice(HttpServletRequest request, String serviceKey) {
        return  apiServiceClient.notice(request,serviceKey);
    }

    /**
     * 组装请求报文头
     * @return
     */
    private AllinpayRequestInfo getRequestInfo(String serviceKey,String version,String bizOrderNo) {
        AllinpayRequestInfo requestInfo = new AllinpayRequestInfo();
        requestInfo.setMerchantId(properties.getPartnerId());
        requestInfo.setUserName(properties.getPartnerName());
        requestInfo.setUserPass(properties.getPartnerPassword());
        requestInfo.setVersion(version);
        requestInfo.setTrxCode(serviceKey);
        if(Strings.isNotBlank(bizOrderNo)) {
            requestInfo.setReqSn(bizOrderNo);
        }
        return requestInfo;
    }


}
