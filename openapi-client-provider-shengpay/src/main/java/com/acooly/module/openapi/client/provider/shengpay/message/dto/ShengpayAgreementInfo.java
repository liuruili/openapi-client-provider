package com.acooly.module.openapi.client.provider.shengpay.message.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/18 14:04
 */
@Getter
@Setter
public class ShengpayAgreementInfo implements Serializable {

    /**
     *银行卡绑定ID
     */
    private String agreementNo;

    /**
     *银行卡类型
     */
    private String bankCardType;

    /**
     *银行代码
     */
    private String bankCode;

    /**
     *银行卡号隐码
     */
    private String bankCardNoMark;

    /**
     *商户会员唯一标识
     */
    private String outMemberId;

    /**
     *签约时间，格式（24小时制）：yyyy-MM-dd HH:mm:ss，例：2015-06-09 19:23:05
     */
    private String signTime;
}
