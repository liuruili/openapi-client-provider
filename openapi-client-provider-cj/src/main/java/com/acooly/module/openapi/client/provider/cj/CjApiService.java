/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.cj;


import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchDeductResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchQueryRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjBatchQueryResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjRealDeductRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealDeductResponse;
import com.acooly.module.openapi.client.provider.cj.message.CjRealQueryRequest;
import com.acooly.module.openapi.client.provider.cj.message.CjRealQueryResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author jifeng 2017-09-25 14:40
 */
@Service
public class CjApiService {
	
	@Resource(name = "cjApiServiceClient")
	private CjApiServiceClient cjApiServiceClient;

	@Autowired
	private CjProperties cjProperties;
	
	/**
	 * 单笔代扣
	 * @param request
	 * @return
	 */
	public CjRealDeductResponse cjRealDeduct(CjRealDeductRequest request) {
		request.setServiceCode(CjServiceEnum.CJ_REAL_DEDUCT.getCode());
		return (CjRealDeductResponse) cjApiServiceClient.execute(request);
	}

	/**
	 * 批量代扣
	 * @param request
	 * @return
	 */
	public CjBatchDeductResponse cjBatchDeduct(CjBatchDeductRequest request) {
		request.setServiceCode(CjServiceEnum.CJ_BATCH_DEDUCT.getCode());
		return (CjBatchDeductResponse) cjApiServiceClient.execute(request);
	}

	/**
	 * 单笔代扣查询
	 * @param request
	 * @return
	 */
	public CjRealQueryResponse cjRealDeductQuery(CjRealQueryRequest request) {
		request.setServiceCode(CjServiceEnum.CJ_REAL_DEDUCT_QUERY.getCode());
		return (CjRealQueryResponse) cjApiServiceClient.execute(request);
	}

	/**
	 * 批量代扣查询
	 * @param request
	 * @return
	 */
	public CjBatchQueryResponse cjBatchDeductQuery(CjBatchQueryRequest request) {
		request.setServiceCode(CjServiceEnum.CJ_BATCH_DEDUCT_QUERY.getCode());
		return (CjBatchQueryResponse) cjApiServiceClient.execute(request);
	}
}

