/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_CREATE ,type = ApiMessageType.Request)
public class LoanCreateRequest extends FudianRequest {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 标的金额
     * 标的金额,以元为单位
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 标的名称
     * 标的名称
     */
    @NotEmpty
    @Length(max=256)
    private String loanName;

    /**
     * 标的类型
     * 1表示普通标的， 融资人和资金使用方相同。资金为融资人使用，3表示担保标的， 融资人无法还款的时候，有担保公司代偿还款，代偿必须传此类型，否则无法代偿成功
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String loanType;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 担保方托管用户号
     * 担保公司在存管开立的托管用户号，lonaType=3必填
     */
    @Length(max=32)
    private String vouchUserName;

    /**
     * 担保方托管账户号
     * 担保公司在存管开立的托管帐户号，lonaType=3必填
     */
    @Length(max=32)
    private String vouchAccountNo;

    /**
     * 投标截止日期
     * 投标截止日期yyyyMMdd
     */
    @NotBlank
    @Size(min = 8,max = 8)
    private String endTime;

    /**
     * 年利率
     * 不能超过36%
     */
    @NotBlank
    @Size(max = 10)
    private String interestRate;
}