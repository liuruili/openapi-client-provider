package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhike 2018/3/7 12:03
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.MERCHANT_MERCHANTTRANSFER ,type = ApiMessageType.Response)
public class MerchantTransferResponse extends FudianResponse {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 提现金额
     * 提现金额，以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 参数扩展域
     * 该字段在交易完成后由本平台原样返回。注意：如果该字段中包含了中文字符请对该字段的数据进行Base64加密后再使用
     */
    private String extMark;

    /**
     * 状态，1成功，-1失败
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;
}
