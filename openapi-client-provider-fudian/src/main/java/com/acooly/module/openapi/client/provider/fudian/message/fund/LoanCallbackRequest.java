/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.core.utils.Collections3;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import com.acooly.module.openapi.client.provider.fudian.message.fund.dto.LoanCallbackInvestInfo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOAN_CALLBACK ,type = ApiMessageType.Request)
public class LoanCallbackRequest extends FudianRequest {

    /**
     * 投资人回款列表
     * json格式的投资人回款信息，字段详情见3.5.11.4
     */
    @NotEmpty
    private List<LoanCallbackInvestInfo> investList;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

    @Override
    public void doCheck() {
        super.doCheck();
        if(Collections3.isEmpty(investList)) {
            throw new ApiClientException("投资人回款列表不能为空");
        }
        for (LoanCallbackInvestInfo investInfo:investList) {
            Validators.assertJSR303(investInfo);
        }
    }
}