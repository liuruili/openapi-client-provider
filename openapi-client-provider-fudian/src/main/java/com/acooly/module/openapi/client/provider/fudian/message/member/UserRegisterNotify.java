/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-25 16:43 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhangpu 2018-01-25 16:43
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.USER_REGISTER, type = ApiMessageType.Notify)
public class UserRegisterNotify extends FudianNotify {

    private String accountNo;

    private String identityType = "1";

    private String identityCode;

    private String returnUrl;

    private String notifyUrl;

    private String merchantNo;

    private String mobilePhone;

    private String regDate;

    private String realName;

    private String userName;

    /**
     * 角色类型
     * 角色类型：1借款人 3出借人
     */
    @NotBlank
    private String roleType;

    /**银行名称**/
    private String bank;
    /**银行卡号**/
    private String bankCardNo;
    /**银行代号**/
    private String bankCode;

}
