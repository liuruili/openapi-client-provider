package com.acooly.module.openapi.client.provider.hx.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxRequest;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.request.IssuedTradeReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Ips")
@HxApiMsgInfo(service = HxServiceEnum.hxWithdrawQuery, type = ApiMessageType.Request)
public class HxWithdrawQueryRequest extends HxRequest {

    @XStreamAlias("IssuedTradeReq")
    private IssuedTradeReq issuedTradeReq;
}
