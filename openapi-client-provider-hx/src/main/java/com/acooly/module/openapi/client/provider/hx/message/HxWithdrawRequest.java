package com.acooly.module.openapi.client.provider.hx.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxRequest;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;
import com.acooly.module.openapi.client.provider.hx.message.xStream.common.ReqWithdrawHead;
import com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.request.ReqWithdrawBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Req")
@HxApiMsgInfo(service = HxServiceEnum.hxWithdraw, type = ApiMessageType.Request)
public class HxWithdrawRequest extends HxRequest {

    @XStreamAlias("Head")
    private ReqWithdrawHead reqWithdrawHead;

    @XStreamAlias("Body")
    private ReqWithdrawBody reqWithdrawBody;

}
