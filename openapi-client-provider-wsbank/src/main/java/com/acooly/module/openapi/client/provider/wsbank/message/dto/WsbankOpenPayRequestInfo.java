package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Getter
@Setter
@XStreamAlias("request")
public class WsbankOpenPayRequestInfo implements Serializable{


    /**
     * 请求报文头
     */
    @NotNull
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private WsbankOpenPayRequestBody wsbankOpenPayRequestBody;
}
