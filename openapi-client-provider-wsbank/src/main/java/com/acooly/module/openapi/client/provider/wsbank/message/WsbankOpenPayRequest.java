package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOpenPayRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhenning.yang
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.OPEN_PAY, type = ApiMessageType.Request)
public class WsbankOpenPayRequest extends WsbankRequest {

    @XStreamAlias("request")
    private WsbankOpenPayRequestInfo wsbankOpenPayRequestInfo;

    @Override
    public void doCheck() {
        Validators.assertJSR303(wsbankOpenPayRequestInfo);
        Validators.assertJSR303(wsbankOpenPayRequestInfo.getHeadRequest());
        Validators.assertJSR303(wsbankOpenPayRequestInfo.getWsbankOpenPayRequestBody());
    }
}
