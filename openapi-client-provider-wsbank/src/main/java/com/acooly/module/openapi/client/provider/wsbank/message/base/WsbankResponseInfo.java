package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/5/22 22:12
 */
@Getter
@Setter
@XStreamAlias("RespInfo")
public class WsbankResponseInfo implements Serializable {

    /**
     * 结果状态
     */
    @XStreamAlias("ResultStatus")
    private String resultStatus;

    /**
     * 结果code
     */
    @XStreamAlias("ResultCode")
    private String resultCode;

    /**
     * 结果信息
     */
    @XStreamAlias("ResultMsg")
    private String resultMsg;
}
