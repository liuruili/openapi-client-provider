package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankWithdrawQueryResponseBody implements Serializable {

    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     * 提现商户号
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 外部交易号。合作方系统生成的外部交易号，同一交易号被视为同一笔交易
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     * 网商提现订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 提现总金额
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种
     */
    @XStreamAlias("Currency")
    private String currency;

    /**
     * 平台设置提现手续费用（银行控制盖帽比率），实收资金= TotalAmount - PlatformFee
     */
    @XStreamAlias("PlatformFee")
    private String platformFee;

    /**
     * 币种
     */
    @XStreamAlias("FeeCurrency")
    private String feeCurrency;

    /**
     * 状态
     * (SUCCESS, DEALING ，FAIL)
     */
    @XStreamAlias("Status")
    private String status;

    /**
     * 银行联行号
     */
    @XStreamAlias("ContactLine")
    private String contactLine;

    /**
     * 绑定银行卡号
     */
    @XStreamAlias("BankCardNo")
    private String bankCardNo;

    /**
     * 绑卡卡号户名
     */
    @XStreamAlias("BankCertName")
    private String bankCertName;

    /**
     * 提现申请时间
     */
    @XStreamAlias("WithdrawApplyDate")
    private String withdrawApplyDate;

    /**
     * 提现完成时间
     */
    @XStreamAlias("WithdrawFinishDate")
    private String withdrawFinishDate;

    /**
     * 备注
     */
    @XStreamAlias("Memo")
    private String memo;

}
