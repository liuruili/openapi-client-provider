package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkmerchanttradeRefundRequestBody implements Serializable {

	private static final long serialVersionUID = -4957566653955247068L;

	/**
	 * 外部交易号。合作方系统提交的交易号。
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;

	/**
	 * 商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
	 */
	@Size(max = 64)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;

	/**
	 * 合作方机构号（网商银行分配）
	 */
	@Size(max = 64)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	/**
	 * 退款外部交易号。由合作方生成，同笔退款交易，交易状态未明需要重试时，使用同一个交易号。
	 */
	@Size(max = 64)
	@XStreamAlias("OutRefundNo")
	@NotBlank
	private String outRefundNo;

	/**
	 * 退款金额。币种同原交易
	 */
	@XStreamAlias("RefundAmount")
	@NotBlank
	private String refundAmount;

	/**
	 * 退款原因。支付宝交易须填写。
	 */
	@Size(max = 256)
	@XStreamAlias("RefundReason")
	private String refundReason;

	/**
	 * 操作员ID
	 */
	@Size(max = 32)
	@XStreamAlias("OperatorId")
	private String operatorId;

	/**
	 * 终端设备号(门店号或收银设备ID)。
	 */
	@Size(max = 32)
	@XStreamAlias("DeviceId")
	private String deviceId;

	/**
	 * 创建订单终端的IP
	 */
	@Size(max = 16)
	@XStreamAlias("DeviceCreateIp")
	@NotBlank
	private String deviceCreateIp;

}
