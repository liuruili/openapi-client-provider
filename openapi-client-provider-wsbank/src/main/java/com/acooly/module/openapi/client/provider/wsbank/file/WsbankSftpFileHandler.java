package com.acooly.module.openapi.client.provider.wsbank.file;//package com.acooly.module.openapi.client.provider.wsbank.file;
//
//import com.acooly.core.utils.Strings;
//import com.acooly.module.openapi.client.file.domain.FileHandlerOrder;
//import com.acooly.module.openapi.client.file.impl.AbstractSftpFileHandler;
//import lombok.Getter;
//import lombok.Setter;
//
///**
// * fudian专用的FileHandler实现，采用代码方式初始化
// */
//@Getter
//@Setter
//public class JytSftpFileHandler extends AbstractSftpFileHandler {
//
//    private String localRoot;
//    private String serverRoot;
//
//    @Override
//    protected String getLocalFilePath(FileHandlerOrder order) {
//        return getStoragePath() + "/" + order.getName() + "_" + order.getPeriod();
//    }
//
//    @Override
//    protected String getServerFilePath(FileHandlerOrder order) {
//        return Strings.trimToEmpty(this.serverRoot) + "/" +order.getPeriod();
//    }
//
//
//    protected String getStoragePath() {
//        if (Strings.isBlank(this.localRoot)) {
//            this.localRoot = getSysTmpDir();
//        }
//        return this.localRoot;
//    }
//
//    protected String getSysTmpDir() {
//        return Strings.trimToEmpty(System.getProperty("java.io.tmpdir"));
//    }
//}
