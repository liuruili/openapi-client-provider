package com.acooly.module.openapi.client.provider.cmb;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.cmb.CmbProperties.PREFIX;


@EnableConfigurationProperties({CmbProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class CmbConfigration {

    @Autowired
    private CmbProperties cmbProperties;

    @Bean("cmbHttpTransport")
    public Transport cmbHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(cmbProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(cmbProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(cmbProperties.getReadTimeout()));
        return httpTransport;
    }
}
