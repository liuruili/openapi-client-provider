package com.acooly.module.openapi.client.provider.bosc.enums;

public enum ActivateStatusEnum {

	ACTIVATED("activated", "已激活"),

	NOTACCT("notacct", "不存在的账户"),

	NOTACTIVATE("notactivate", "未激活");

	private String code;
	private String message;

	private ActivateStatusEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
