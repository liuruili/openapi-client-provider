/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月12日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.enums;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhike
 */
public enum SinaTradeStatus {

	PAY_FINISHED("PAY_FINISHED", "已付款"),

	TRADE_FAILED("TRADE_FAILED", "交易失败"),

	TRADE_FINISHED("TRADE_FINISHED", "交易结束"),

	/** 合作方通过调用交易取消接口来关闭(系统会异步通知) */
	TRADE_CLOSED("TRADE_CLOSED", "交易关闭"),

	PRE_AUTH_APPLY_SUCCESS("PRE_AUTH_APPLY_SUCCESS", "代收冻结成功"),

	PRE_AUTH_CANCELED("PRE_AUTH_CANCELED", "代收撤销成功"),
	
	FINISHED("FINISHED", "批量代收完成");

	public static boolean isSuccess(String code) {
		return find(code) == TRADE_FINISHED;
	}

	private final String code;
	private final String message;

	private SinaTradeStatus(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String code() {
		return code;
	}

	public String message() {
		return message;
	}

	public static Map<String, String> mapping() {
		Map<String, String> map = Maps.newLinkedHashMap();
		for (SinaTradeStatus type : values()) {
			map.put(type.getCode(), type.getMessage());
		}
		return map;
	}

	/**
	 * 通过枚举值码查找枚举值。
	 * 
	 * @param code
	 *            查找枚举值的枚举值码。
	 * @return 枚举值码对应的枚举值。
	 * @throws IllegalArgumentException
	 *             如果 code 没有对应的 Status 。
	 */
	public static SinaTradeStatus find(String code) {
		for (SinaTradeStatus status : values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("SinaTradeType not legal:" + code);
	}

	/**
	 * 获取全部枚举值。
	 * 
	 * @return 全部枚举值。
	 */
	public static List<SinaTradeStatus> getAll() {
		List<SinaTradeStatus> list = new ArrayList<SinaTradeStatus>();
		for (SinaTradeStatus status : values()) {
			list.add(status);
		}
		return list;
	}

	/**
	 * 获取全部枚举值码。
	 * 
	 * @return 全部枚举值码。
	 */
	public static List<String> getAllCode() {
		List<String> list = new ArrayList<String>();
		for (SinaTradeStatus status : values()) {
			list.add(status.code());
		}
		return list;
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.code, this.message);
	}
}
