package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.SinapayConstants;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiTransient;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 交易支付
 *
 * @author xiaohong
 * @create 2018-07-17 9:54
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.PAY_HOSTING_TRADE, type = ApiMessageType.Request)
public class PayHostingTradeRequest extends SinapayRequest {

    /**
     * 支付请求号
     *
     * 商户网站支付订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_pay_no")
    private String outPayNo = Ids.oid();

    /**
     * 商户网站唯一交易订单号集合
     *
     * 钱包合作商户网站唯一订单号(确保在合作伙伴系统中唯一)。总数不超过十笔
     * 格式：2013112405052132^2013112405052233
     */
    @NotEmpty
    @Size(max = 500)
    @ApiItem(value = "outer_trade_no_list")
    private String outerTradeNoList;

    /**
     * 付款用户IP地址
     *
     * 用户在商户平台发起支付时候的IP地址，公网IP，不是内外IP
     * 用于风控校验，请填写用户真实IP，否则容易风控拦截
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "payer_ip")
    private String payerIp;

    /**
     * 支付方式
     *
     * 格式：支付方式^金额^扩展|支付方式^金额^扩展。
     * 扩展信息内容以“，”分隔，针对不同支付方式的扩展定义见附录 “支付方式扩展”
     */
    @Size(max = 1000)
    @ApiItem(value = "pay_method")
    private String payMethod;

    @MoneyConstraint
    @ApiTransient
    private Money amount;

    /**
     * 新浪支付方式
     */
    @ApiTransient
    @NotNull
    private SinapayMode payMode = SinapayMode.ONLINE_BANK;
    @ApiTransient
    private SinapayBankCode bankCode = SinapayBankCode.SINAPAY;
    @ApiTransient
    private SinapayCardType cardType = SinapayCardType.DEBIT;
    /** 卡属性 */
    @ApiTransient
    private SinapayCardAttribute cardAttribute = SinapayCardAttribute.C;
    @ApiTransient
    @NotNull
    private SinapayAccountType accountType = SinapayAccountType.SAVING_POT;

    public String getPayMethod() {
        StringBuilder sb = new StringBuilder();
        if (payMode == SinapayMode.ONLINE_BANK) {
            setReturnUrl(SinapayConstants.GATHER_RETURN_URL);
            sb.append(payMode.code()).append("^").append(this.amount).append("^").append(this.bankCode.code())
                    .append(",").append(this.cardType.code()).append(",").append(this.cardAttribute.code());
        } else if (payMode == SinapayMode.BALANCE) {
            sb.append(payMode.code()).append("^").append(this.amount).append("^").append(this.accountType.code());
        }
        return sb.toString();
    }
}
