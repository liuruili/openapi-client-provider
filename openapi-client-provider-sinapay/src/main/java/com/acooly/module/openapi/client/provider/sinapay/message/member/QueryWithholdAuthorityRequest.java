/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 委托扣款签约 查询
 * 
 * 委托扣款签约的同步通知回来后根据identify查询结果
 * 
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_WITHHOLD_AUTHORITY, type = ApiMessageType.Request)
public class QueryWithholdAuthorityRequest extends SinapayRequest {

	/**
	 * 用户标识信息
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "identity_id")
	private String identityId;

	/**
	 * 交易码
	 *
	 * 商户网站代收交易业务码，见附录
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "identity_type")
	private String identityType = "UID";

	/**
	 * 是否展示授权明细
	 * 
	 * 是否展示授权明细（Y:是 N：否），默认不展示
	 */
	@Size(max = 1)
	@ApiItem(value = "is_detail_disp")
	private String isDetailDisp;

	/**
	 * 授权类型
	 * 
	 * 授权类型,ALL银行卡授权（该授权包含账户授权） ACCOUNT: 账户授权
	 */
	@Size(max = 200)
	@ApiItem(value = "auth_type")
	private String authType = "ACCOUNT";
}
