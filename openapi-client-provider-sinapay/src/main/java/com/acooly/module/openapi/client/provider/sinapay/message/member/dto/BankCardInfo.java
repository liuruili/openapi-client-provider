/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月30日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member.dto;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike
 */
@ApiDto
@Getter
@Setter
public class BankCardInfo implements Dtoable {

	@ItemOrder(0)
	private String cardId;
	@ItemOrder(1)
	private String bankCode;
	@ItemOrder(2)
	private String bankAccountNo;
	@ItemOrder(3)
	private String accountName;
	@ItemOrder(4)
	private String cardType;
	@ItemOrder(5)
	private String cardAttribute;

	/** VerifyMode是否是Sign 绑卡时如果verify_mode 选择是SIGN,则返回Y，否则返回N */
	@ItemOrder(6)
	private String verifyMode;

	/** 创建时间 */
	@ItemOrder(7)
	private String createTime;

	/** 安全卡标识 银行卡是否为安全卡，Y：安全卡；N：非安全卡 */
	@ItemOrder(8)
	private String securifyFlag;

	@Override
	public String toString() {
		return ToString.toString(this);
	}

}
