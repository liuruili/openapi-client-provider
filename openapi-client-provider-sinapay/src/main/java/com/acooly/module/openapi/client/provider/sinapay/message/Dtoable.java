/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message;

import java.io.Serializable;

/**
 * 标志接口，标志实现类似报文DTO对象
 * 
 * @author zhike
 */
public interface Dtoable extends Serializable {

}
