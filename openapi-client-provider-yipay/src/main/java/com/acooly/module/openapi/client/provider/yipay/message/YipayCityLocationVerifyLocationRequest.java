package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.CITY_LOCATION_VERIFY_LOCATION,type = ApiMessageType.Request)
public class YipayCityLocationVerifyLocationRequest extends YipayRequest {

    /**
     * 联系方式:手机号码
     * 必须是11位电信或者联通的手机号码
     */
    @NotBlank
    @Size(min = 11,max = 11)
    @YipayAlias(value = "mobile")
    private String mobile;

    /**
     * 地区编码
     *參考枚举com.acooly.module.openapi.client.provider.yipay.enums.YipayAreaCodeEnum
     */
    @NotBlank
    @Size(max = 6)
    @YipayAlias(value = "areaCode")
    private String areaCode;
}
