package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengTradeQueryInfo;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.TRADE_ORDER_QUEERY, type = ApiMessageType.Request)
public class YinShengTradeOrderQueryRequest extends YinShengRequest {

    /**
     * 查询业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengTradeQueryInfo yinShengTradeQueryInfo;

}
