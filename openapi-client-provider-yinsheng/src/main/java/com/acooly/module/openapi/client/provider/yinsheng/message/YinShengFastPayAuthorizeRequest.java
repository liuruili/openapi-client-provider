package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengFastPayAuthorizeInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.FAST_PAY_AUTHORIZE, type = ApiMessageType.Request)
public class YinShengFastPayAuthorizeRequest extends YinShengRequest{
    /**
     * 快捷认证业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengFastPayAuthorizeInfo yinShengFastPayAuthorizeInfo;
}
