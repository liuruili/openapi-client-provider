package com.acooly.module.openapi.client.provider.yinsheng.message.netbank;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/8 16:41
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.SCAN_PAY, type = ApiMessageType.Response)
public class YinShengScanPayResponse extends YinShengResponse{

    /**
     *订单号
     * 银盛支付合作商户网站唯一订单号。
     */
    private String out_trade_no;

    /**
     *银盛流水号
     * 银盛支付平台的交易流水
     */
    private String trade_no;

    /**
     *状态码
     * 交易目前所处的状态。成功状态的值： TRADE_SUCCESS|TRADE_CLOSED等具体详情看错误码中的交易状态详解
     */
    private String trade_status;

    /**
     *订单总金额
     * 该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
     */
    private String total_amount;

    /**
     *日期
     * 格式"yyyy-MM-dd"
     */
    private String account_date;

    /**
     * 二维码地址
     * 该地址打开就是一个二维码可扫可支付
     */
    private String qr_code_url;

    /**
     * 银行类型
     * 二维码行别
     */
    private String bank_type;

    /**
     * 超时时间
     * 过期时间单位分钟
     */
    private String 	expire_time;

    /**
     * 源二维码地址
     */
    private String source_qr_code_url;
}
