/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum YinShengScanPayBankTypeEnum implements Messageable {

    ALI_SCAN_PAY("1903000", "支付宝APP支付"),
    WECHAT_SCAN_PAY("1902000", "微信APP支付"),
    QQ_SCAN_PAY("1904000", "微信APP支付"),
    ;

    private final String code;
    private final String message;

    private YinShengScanPayBankTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (YinShengScanPayBankTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YinShengScanPayBankTypeEnum find(String code) {
        for (YinShengScanPayBankTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YinShengScanPayBankTypeEnum> getAll() {
        List<YinShengScanPayBankTypeEnum> list = new ArrayList<YinShengScanPayBankTypeEnum>();
        for (YinShengScanPayBankTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YinShengScanPayBankTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
