package com.acooly.module.openapi.client.provider.baofup.marshall;

import com.acooly.module.openapi.client.api.exception.ApiClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author weichk
 */
@Slf4j
@Service
public class BaoFuPSignMarshall extends BaoFuPMarshallSupport {


    /**
     * 将收到的报文转化为map
     *
     * @param request
     * @return
     */
    public Map<String, String> getDateMap(HttpServletRequest request) {
        try {
            request.setCharacterEncoding("utf-8");
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
                String name = entry.getKey();
                String[] values = entry.getValue();
                String valueStr = "";
                if (values != null) {
                    for (int i = 0; i < values.length; i++) {
                        valueStr = (i == values.length - 1) ? valueStr + values[i]
                                : valueStr + values[i] + ",";
                    }

                    params.put(name, valueStr);
                }

            }
            return params;
        } catch (Exception e) {
            throw new ApiClientException("签名失败", e);
        }
    }
}
