package com.acooly.module.openapi.client.provider.yl.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yl.domain.YlApiMsgInfo;
import com.acooly.module.openapi.client.provider.yl.domain.YlResponse;
import com.acooly.module.openapi.client.provider.yl.enums.YlServiceEnum;

import java.io.File;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YlApiMsgInfo(service = YlServiceEnum.YL_BILL_DOWNLOAD, type = ApiMessageType.Response)
public class YlbillDownloadResponse extends YlResponse {
    /**
     * 对帐文件
     */
    private File file;
}
