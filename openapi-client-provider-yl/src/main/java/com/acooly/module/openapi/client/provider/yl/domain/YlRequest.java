/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.yl.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class YlRequest extends YlApiMessage {

    /**
     * 交易流水号（唯一）   30位
     */
    private String reqSn;

    /**
     * 交易代码   （  批量代收取值 100001    实时代收取值  100004）
     */
    private String trxCode;
    /**
     * 版本（05）
     */
    private String version;
    /**
     * 数据格式   取值 2：xml格式
     */
    private String dataType;
    /**
     * 处理级别 0-9  0实时处理     默认5  目前暂时只提供批量处理？
     */
    private String level;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户密码
     */
    private String userPass;
}
