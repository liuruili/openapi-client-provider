package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike@acooly.com
 * @date 2018-09-07 16:00
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_REFUND_QUERY, type = ApiMessageType.Request)
public class FbankTradeRefundQueryRequest extends FbankRequest {

    /**
     * 商户退款订单号
     * 商户退款订单号，如果存在，则只返回此笔退款记录
     */
    @Size(max = 50)
    private String refMchntOrderNo;

    /**
     * 原交易商户订单号
     * 商户自己平台的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 原交易银行订单号
     * 支付平台生成的订单号（唯一）
     * mchntOrderNo与 orderNo必须传一个
     */
    @Size(max = 32)
    private String orderNo;

    @Override
    public void doCheck() {
        super.doCheck();
        if(Strings.isBlank(mchntOrderNo)&&Strings.isBlank(orderNo)) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"mchntOrderNo和orderNo不能同时为空");
        }
    }
}
