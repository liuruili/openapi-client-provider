/**
 * create by zhike
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.fbank;

import com.acooly.module.openapi.client.provider.fbank.domain.FbankNotify;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import com.acooly.module.openapi.client.provider.fbank.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike
 */
@Service
public class FbankApiService {

    @Resource(name = "fbankApiServiceClient")
    private FbankApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientFbankProperties openAPIClientFbankProperties;


    /**
     * 聚合支付接口
     *
     * @param request
     * @return
     */
    public FbankPrepayResponse prepay(FbankPrepayRequest request) {
        request.setNotifyUrl(FbankConstants.getCanonicalUrl(openAPIClientFbankProperties.getDomain(),
                openAPIClientFbankProperties.getNotifyUrl()
                        + FbankServiceEnum.PREPAY_API.getKey()));
        return (FbankPrepayResponse) apiServiceClient.execute(request);
    }

    /**
     * 交易退款接口
     *
     * @param request
     * @return
     */
    public FbankTradeRefundResponse tradeRefund(FbankTradeRefundRequest request) {
        request.setRefundNotifyUrl(FbankConstants.getCanonicalUrl(openAPIClientFbankProperties.getDomain(),
                openAPIClientFbankProperties.getNotifyUrl()
                        + FbankServiceEnum.TRADE_REFUND.getKey()));
        request.setService(FbankServiceEnum.TRADE_REFUND.getKey());
        return (FbankTradeRefundResponse) apiServiceClient.execute(request);

    }

    /**
     * 交易退款查询接口
     *
     * @param request
     * @return
     */
    public FbankTradeRefundQueryResponse tradeRefundQuery(FbankTradeRefundQueryRequest request) {
        request.setService(FbankServiceEnum.TRADE_REFUND_QUERY.getKey());
        return (FbankTradeRefundQueryResponse) apiServiceClient.execute(request);

    }


    /**
     * 交易撤销接口
     *
     * @param request
     * @return
     */
    public FbankTradeCancelResponse tradeCancel(FbankTradeCancelRequest request) {
        request.setService(FbankServiceEnum.TRADE_CANCEL.getKey());
        return (FbankTradeCancelResponse) apiServiceClient.execute(request);
    }

    /**
     * 交易关闭接口
     *
     * @param request
     * @return
     */
    public FbankTradeCloseResponse tradeClose(FbankTradeCloseRequest request) {
        request.setService(FbankServiceEnum.TRADE_CLOSE.getKey());
        return (FbankTradeCloseResponse) apiServiceClient.execute(request);
    }

    /**
     * 交易订单查询
     *
     * @param request
     * @return
     */
    public FbankTransQueryResponse transQuery(FbankTransQueryRequest request) {
        request.setService(FbankServiceEnum.TRADE_QUERY.getKey());
        return (FbankTransQueryResponse) apiServiceClient.execute(request);

    }

    /**
     * 获取对账文件下载地址
     *
     * @param request
     * @return
     */
    public FbankReconFileUrlQueryResponse reconFileUrlQuery(FbankReconFileUrlQueryRequest request) {
        request.setService(FbankServiceEnum.TRADE_DOWNLOAD_FILE.getKey());
        return (FbankReconFileUrlQueryResponse) apiServiceClient.execute(request);
    }

    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public FbankNotify notice(HttpServletRequest request, String serviceKey) {
        return apiServiceClient.notice(request, serviceKey);
    }
}
