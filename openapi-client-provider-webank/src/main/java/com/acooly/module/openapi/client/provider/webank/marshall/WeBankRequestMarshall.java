/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.webank.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class WeBankRequestMarshall extends WeBankMarshallSupport implements ApiMarshal<String, WeBankRequest> {

    @Override
    public String marshal(WeBankRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
