/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.webank.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Fuiou 响应报文基类
 *
 *
 * @author zhangpu
 */
@Getter
@Setter
public class WeBankResponse extends WeBankApiMessage {

	private String status;

	/**
	 * 响应码
	 */
	String respCode;

	/**
	 * 响应码描述
	 */
	String respMsg;
}
