package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuInfoMessage;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/29 17:22
 */
@Getter
@Setter
public class BaoFuBatchDeductInfo extends BaoFuInfoMessage {
    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字，
     * 注：支付请求中trans_id作为主键，如果有重复的交易请求存在，以第一个成功交易为准，后续重复交易不被受理。
     * 不支持商户提交重复订单号，对重复订单号将直接提示订单已经提交。
     */
    @NotBlank
    @BaoFuAlias(value = "trans_id", request = false)
    @JSONField(ordinal = 1)
    private String transId;

    /**
     * 订单日期 14 位定长。
     * 格式：年年年年月月日日时时分分秒秒
     */
    @NotBlank
    @BaoFuAlias(value = "trade_date", request = false)
    @JSONField(ordinal = 2)
    private String tradeDate;

    /**
     * 卡号
     * 整型数字
     * 提交给宝付的银行卡卡号
     */
    @NotBlank
    @BaoFuAlias(value = "acc_no", request = false)
    @JSONField(ordinal = 3)
    private String accNo;

    /**
     * 持卡人姓名 提交给宝付的持卡人姓名
     */
    @NotBlank
    @BaoFuAlias(value = "id_holder", request = false)
    @JSONField(ordinal = 4)
    private String idHolder;

    /**
     * 身份证类型
     * 固定值：01
     * 01认为身份证号
     */
    @BaoFuAlias(value = "id_card_type", request = false)
    @JSONField(ordinal = 5)
    private String idCardType = "01";

    /**
     * 身份证号 提交给宝付的持卡人身份证号
     */
    @NotBlank
    @BaoFuAlias(value = "id_card", request = false)
    @JSONField(ordinal = 6)
    private String idCard;

    /**
     * 银行卡绑定手机号 提交给宝付的持卡人预留手机号（四要素验证时必传）
     */
    @BaoFuAlias(value = "mobile", request = false)
    @JSONField(ordinal = 7)
    private String mobile="";

    /**
     * 交易金额 单位：分
     */
    @NotBlank
    @BaoFuAlias(value = "txn_amt", request = false)
    @JSONField(ordinal = 8)
    private String txnAmt;

    /**
     * 请求方保留域
     */
    @BaoFuAlias(value = "req_reserved")
    @JSONField(ordinal = 9)
    private String reqReserved="";

}
