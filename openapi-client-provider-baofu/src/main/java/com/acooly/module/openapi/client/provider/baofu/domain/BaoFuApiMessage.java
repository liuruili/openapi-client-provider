/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.baofu.domain;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Strings;
import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuDataTypeEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike
 */
@Getter
@Setter
public class BaoFuApiMessage implements ApiMessage {

    /**
     * 服务码
     */
    @XStreamOmitField
    private String service;

    /**
     * 服务版本
     */
    @NotBlank(message = "服务版本不能为空")
    @BaoFuAlias(value = "version", sign = false)
    @XStreamAlias("version")
    private String version = "4.0.0.0";

    /**
     * 终端号
     */
    @NotBlank(message = "终端号不能为空")
    @BaoFuAlias(value = "terminal_id")
    @XStreamAlias("terminal_id")
    private String terminalId;

    /**
     * 交易类型
     */
    @XStreamAlias("txn_type")
    @BaoFuAlias(value = "txn_type", sign = false)
    private String txnType;

    /**
     * 子交易类型
     */
    @BaoFuAlias(value = "txn_sub_type")
    private String txnSubType;

    /**
     * 商户号
     */
    @BaoFuAlias(value = "member_id")
    @NotBlank(message = "商户号不能为空")
    @XStreamAlias("member_id")
    private String memberId;

    /**
     * 加密数据类型
     */
    @BaoFuAlias(value = "data_type", sign = false)
    @XStreamAlias("data_type")
    private String dataType = BaoFuDataTypeEnum.xml.getCode();

    /**
     * 加密数据
     */
    @BaoFuAlias(value = "data_content", sign = false)
    @XStreamAlias("data_content")
    private String dataContent;

    /**
     * 请求url
     */
    @BaoFuAlias(value = "gatewayUrl", sign = false, request = false)
    @XStreamOmitField
    private String gatewayUrl;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getService() {
        return service;
    }

    @Override
    public String getPartner() {
        return memberId;
    }

    public void doCheck() {
        if (!Strings.equals(getService(), BaoFuServiceEnum.BILL_DOWNLOAD.getCode()) && !Strings.equals(getService(), BaoFuServiceEnum.WITHDRAW.getCode()) &&
                !Strings.equals(getService(), BaoFuServiceEnum.WITHDRAW_QUERY.getCode())&&!Strings.equals(getService(), BaoFuServiceEnum.ACCOUNT_BALANCE_QUERY.getCode())) {
            if (Strings.isBlank(getTxnType())) {
                throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "交易类型不能为空");
            }
            if (Strings.isBlank(getTxnSubType())) {
                throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(), "子交易类型不能为空");
            }
        }
    }
}
