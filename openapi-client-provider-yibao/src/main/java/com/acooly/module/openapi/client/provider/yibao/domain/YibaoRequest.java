/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.yibao.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhangpu 2018-01-23 12:58
 */
@Getter
@Setter
public class YibaoRequest extends YibaoMessage {

    /**
     * 请求流水号
     */
    @NotBlank
    @Size(max = 64)
    @YibaoAlias(value = "requestno")
    private String orderNo;

    @Length(max = 256)
    @YibaoAlias(value = "returnUrl")
    private String returnUrl;

    /**
     * 异步回调地址
     * 服务器通知业务参数，请根据此做业务处理
     */
    @Length(max = 256)
    @YibaoAlias(value = "callbackurl")
    private String notifyUrl;
}
