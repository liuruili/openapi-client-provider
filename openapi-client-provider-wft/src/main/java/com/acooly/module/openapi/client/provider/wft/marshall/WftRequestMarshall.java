/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.wft.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.utils.SignUtils;
import com.acooly.module.openapi.client.provider.wft.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.SortedMap;

/**
 * @author zhangpu
 */
@Service
@Slf4j
public class WftRequestMarshall extends WftMarshallSupport implements ApiMarshal<String, WftRequest> {

    private static final Logger logger = LoggerFactory.getLogger(WftRequestMarshall.class);

    @Override
    public String marshal(WftRequest source) {
        beforeMarshall(source);
        SortedMap<String, String> requestDataMap = doMarshall(source);
        String requestDate = XmlUtils.parseXML(requestDataMap);
        return requestDate;
    }

}
