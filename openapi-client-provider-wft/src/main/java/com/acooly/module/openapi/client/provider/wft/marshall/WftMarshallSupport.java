/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu
 * date:2016年4月4日
 *
 */
package com.acooly.module.openapi.client.provider.wft.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.wft.OpenAPIClientWftProperties;
import com.acooly.module.openapi.client.provider.wft.WftConstants;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMessage;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import com.acooly.module.openapi.client.provider.wft.utils.JsonMarshallor;
import com.acooly.module.openapi.client.provider.wft.utils.SignUtils;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.key.KeyLoadManager;
import com.acooly.module.safety.signature.SignTypeEnum;
import com.acooly.module.safety.signature.SignerFactory;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * @author zhangpu
 */
@Slf4j
public class WftMarshallSupport {

    @Autowired
    protected OpenAPIClientWftProperties openAPIClientWftProperties;

    @Autowired
    protected SignerFactory signerFactory;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;

    private static JsonMarshallor jsonMarshallor = JsonMarshallor.INSTANCE;

    protected OpenAPIClientWftProperties getProperties() {
        return openAPIClientWftProperties;
    }

    public SignerFactory getSignerFactory() {
        return signerFactory;
    }

    protected SortedMap<String, String> doMarshall(WftRequest source) {
        doVerifyParam(source);
        SortedMap<String, String> requestDataMap = getRequestDataMap(source);
        log.debug("待签字符串:{}", requestDataMap);
        source.setSign(doSign(requestDataMap, source));
        requestDataMap.put(WftConstants.SIGN, source.getSign());
        return requestDataMap;
    }

    /**
     * 获取代签map
     *
     * @param source
     * @return
     */
    protected SortedMap<String, String> getRequestDataMap(WftRequest source) {
        SortedMap<String, String> requestData = Maps.newTreeMap();
        Set<Field> fields = Reflections.getFields(source.getClass());
        String key = null;
        Object value = null;
        for (Field field : fields) {
            value = Reflections.getFieldValue(source, field.getName());
            if (value == null) {
                continue;
            }
            WftAlias wftAlias = field.getAnnotation(WftAlias.class);
            if (wftAlias == null) {
                continue;
            } else {
                if (!wftAlias.sign()) {
                    continue;
                }
                key = wftAlias.value();
            }
            if (Strings.isNotBlank((String) value)) {
                requestData.put(key, (String) value);
            }
        }
        return requestData;
    }

    /**
     * 将对象转化为String
     *
     * @param object
     * @return
     */
    private String convertString(Object object) {
        if (object == null) {
            return null;
        }
        return object.toString();
    }

    /**
     * 校验参数
     *
     * @param source
     */
    protected void doVerifyParam(WftApiMessage source) {
        try {
            source.doCheck();
            Validators.assertJSR303(source);
        } catch (Exception e) {
            throw new ApiClientException(e.getMessage());
        }
    }

    /**
     * 签名 优先获取传进来的，如果传进来没有则用配置文件中的
     *
     * @param waitForSign
     * @param source
     * @return
     */
    protected String doSign(SortedMap<String, String> waitForSign, WftRequest source) {
        String key = getKeyInfo(source);
        String signStr = Safes.getSigner(WftConstants.SIGNER_TYPE).sign(waitForSign, key);
        return signStr;
    }

    protected String doSign(SortedMap<String, String> waitForSign, String key) {
        String signStr = Safes.getSigner(WftConstants.SIGNER_TYPE).sign(waitForSign, key);
        return signStr;
    }

    /**
     * 验签
     *
     * @param signature
     * @param plain
     * @return
     */
    protected void doVerifySign(SortedMap<String, String> plain, String signature, String partnerId) {
        Safes.getSigner(WftConstants.SIGNER_TYPE).verify(plain, getKeyInfo(partnerId), signature);
    }

    /**
     * 获取key
     *
     * @return
     */
    protected String getKeyInfo(WftRequest source) {
        try {
            return keyStoreLoadManager.load(source.getMchId(), WftConstants.PROVIDER_NAME);
        } catch (Exception e) {
            log.info("商户partnerId={}密钥加载失败,启用配置文件密钥", source.getMchId());
            return openAPIClientWftProperties.getPrivateKey();
        }
    }

    /**
     * 获取keyStoreInfo
     *
     * @return
     */
    protected String getKeyInfo(String partnerId) {
        try {
            return keyStoreLoadManager.load(partnerId, WftConstants.PROVIDER_NAME);
        } catch (Exception e) {
            log.info("商户partnerId={}密钥加载失败,启用配置文件密钥", partnerId);
            return openAPIClientWftProperties.getPrivateKey();
        }
    }

    protected void beforeMarshall(WftApiMessage message) {
        if (Strings.isBlank(message.getService())) {
            message.setService(getProperties().getPartnerId());
        }
    }
}
