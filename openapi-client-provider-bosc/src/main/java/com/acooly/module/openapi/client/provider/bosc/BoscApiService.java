/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.bosc;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.RechargeRequest;
import com.acooly.module.openapi.client.provider.bosc.message.fund.WithdrawRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.EnterpriseRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalRegisterNotify;
import com.acooly.module.openapi.client.provider.bosc.message.member.PersonalRegisterRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationRequest;
import com.acooly.module.openapi.client.provider.bosc.message.member.QueryUserInfomationResponse;
import com.acooly.module.openapi.client.provider.bosc.message.trade.EstablishProjectResponse;
import com.acooly.module.openapi.client.provider.bosc.message.trade.UserAutoPreTransactionRequest;
import com.acooly.module.openapi.client.provider.bosc.message.trade.UserAutoPreTransactionResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Component
public class BoscApiService {
	
	@Resource(name = "boscApiServiceClient")
	private BoscApiServiceClient boscApiServiceClient;
	
	/**
	 * 个人用户注册(跳转)
	 *
	 * @param request
	 *
	 * @return
	 */
	public String personalRegisterRedirect (PersonalRegisterRequest request) {
		return boscApiServiceClient.redirectGet (request);
	}
	
	
	/**
	 * 企业用户注册(跳转)
	 *
	 * @param request
	 *
	 * @return
	 */
	public String enterpriseRegisterRedirect (EnterpriseRegisterRequest request) {
		return boscApiServiceClient.redirectGet (request);
	}
	
	/**
	 * 个人用户注册 返回（同步和异步相同）
	 *
	 * @param data
	 *
	 * @return
	 */
	public PersonalRegisterNotify personalRegisterNotice (Map<String, String> data) {
		return (PersonalRegisterNotify) boscApiServiceClient
				.notice (data, BoscServiceNameEnum.PERSONAL_REGISTER_EXPAND.code ());
	}
	
	/**
	 * 封装通用的异步通知转化
	 *
	 * @param data
	 * @param <T>
	 *
	 * @return
	 */
	public <T> T commonNotifyParser (Map<String, String> data) {
		String service = data.get (BoscConstants.SERVICE_NAME);
		return (T) boscApiServiceClient.notice (data, service);
	}
	
	/**
	 * 会员信息查询
	 *
	 * @param request
	 *
	 * @return
	 */
	public QueryUserInfomationResponse queryUserInfomation (QueryUserInfomationRequest request) {
		return (QueryUserInfomationResponse) boscApiServiceClient.execute (request);
	}
}
