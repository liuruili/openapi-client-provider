/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class PreTransactionDetailInfo {
	
	/**
	 * 见【预处理业务类型】
	 */
	@NotEmpty
	@Size(max=50)
	private String bizType;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max=50)
	private String platformUserNo;
	/**
	 * 预处理冻结金额
	 */
	@MoneyConstraint
	private Money freezeAmount;
	/**
	 * 累计已解冻金额
	 */
	@MoneyConstraint
	private Money unfreezeAmount;
	/**
	 * 已取消金额
	 */
	@MoneyConstraint
	private Money cancelAmount;
	/**
	 * INIT 表示初始化，FREEZED 表示冻结成功, UNFREEZED 表示全部解冻，FAIL 表示 冻结失败，ERROR 表示异常
	 */
	@NotEmpty
	@Size(max=50)
	private String status;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * 交易完成时间，预处理冻结金额全部确认的时间
	 */
	private Date transactionTime;
	
	public String getBizType () {
		return bizType;
	}
	
	public void setBizType (String bizType) {
		this.bizType = bizType;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public Money getFreezeAmount () {
		return freezeAmount;
	}
	
	public void setFreezeAmount (Money freezeAmount) {
		this.freezeAmount = freezeAmount;
	}
	
	public Money getUnfreezeAmount () {
		return unfreezeAmount;
	}
	
	public void setUnfreezeAmount (Money unfreezeAmount) {
		this.unfreezeAmount = unfreezeAmount;
	}
	
	public Money getCancelAmount () {
		return cancelAmount;
	}
	
	public void setCancelAmount (Money cancelAmount) {
		this.cancelAmount = cancelAmount;
	}
	
	public String getStatus () {
		return status;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
}
