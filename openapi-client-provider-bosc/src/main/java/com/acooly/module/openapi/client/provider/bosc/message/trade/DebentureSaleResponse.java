package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscDebentureStatusEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.DEBENTURE_SALE, type = ApiMessageType.Response)
public class DebentureSaleResponse extends BoscResponse {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 债权出让状态，ONSALE（出让中）
	 */
	@NotNull
	private BoscDebentureStatusEnum debentureStatus;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public BoscDebentureStatusEnum getDebentureStatus () {
		return debentureStatus;
	}
	
	public void setDebentureStatus (
			BoscDebentureStatusEnum debentureStatus) {
		this.debentureStatus = debentureStatus;
	}
}