package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_AUTHORIZATION_ENTRUST_PAY_RECORD, type = ApiMessageType.Request)
public class QueryAuthorizationEntrustPayRecordRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	
	
	public QueryAuthorizationEntrustPayRecordRequest () {
		setService (BoscServiceNameEnum.QUERY_AUTHORIZATION_ENTRUST_PAY_RECORD.code ());
	}
	
	public QueryAuthorizationEntrustPayRecordRequest (String requestNo) {
		this();
		this.requestNo = requestNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
}